**Building Custom.xsa, FSBL and U-boot for SPEC7**

Zynq-7000 family, QSPI- mt25ql128_x4_dual -stacked / mt25ql256_x8_dual-parallel

1] The first design implemented for spec7 to generate .xsa and enabling PS (Processing system). This was design created by selecting XCZ030-fbg676-1 part.
     This IP was later added with AXI-GPIO. For successful build, added a .xdc file as well. 
     After running synthesis, implementation and generate bitstream, export design as export hardware as .xsa file.

2] Building FSBL(First Stage Bootloader) 
    Using xsct command line, first intialize the workspace  
```
setws /<path>/xsct_ws
```
  Initialize the platform, indicating -proc as ps7_cortexa9_0
```
platform create -name spec7_custom -hw /<path to xsa>/spec7_custom.xsa -no-boot-bsp
```

Check the active platform:
```
platform active
```

Create domain 
```
domain create -name "fsbl_domain" -os standalone -proc ps7_cortexa9_0
```

We can check the active domain with:
```
domain active
```

And the complete list of domains in the active platform with (an asterisk marks the active one):
```
domain list
```

Add library before building FSBL:
```
bsp setlib xilffs
```

Before moving forward, we can check the parameters for our Operating System in the BSP with:
```
bsp listparams -os
```

Check stdin and stdout configuration in the BSP:
```
bsp config stdin ps7_uart_1
bsp config stdout ps7_uart_1
```

build the platform with the following command:
```
platform generate
```

Now, add system project 

First, you can retrieve the complete list of available templates with:
```
repo -apps
```

First, we create the application for the FSBL targeting the already existing platform and the specific domain and specifying the template Zynq FSBL. In addition, we will select the spec7_custom_system as the name of the system that will be created to host the application:
```
app create -name zynq_fsbl -template {Zynq  FSBL} -platform spec7_custom -domain fsbl_domain -sysproj spec7_custom_system
```

Configuring and Building:
Once the applications are created, we can get help on how to configure them with:
```
app config -help
```

Changing Build configs to release from default:
```
app config -name zynq_fsbl build-config release
```

Check the build config:
```
app config -name zynq_fsbl build-config
```

Proceed with build:
```
app build -name zynqmp_fsbl
```

Creating .bif file for Bootgen:
```
vi xsct_flow.bif
```

Contents of .bif file for SPEC7
```
/* Linux */
the_ROM_image:
{
    [bootloader] xsct_ws/zynq_fsbl/Release/zynq_fsbl.elf
    xsct_ws/spec7_custom/hw/spec7_custom.bit
}

```

For adding and tweaking with more .bif attributes, refer - https://www.xilinx.com/support/documentation/sw_manuals/xilinx2018_2/ug1283-bootgen-user-guide.pdf

Generate the Boot Image with the bootgen command line tool:
```
bootgen -image xsct_flow.bif -arch zynq -w -o BOOT.bin
```

3] U-boot for SPEC7: 

Important dependencies and environment variables required for U-boot:
    1. gcc-arm-linux-gnu-gnueabihf - [Sources] - https://releases.linaro.org/components/toolchain/binaries/7.5-2019.12/arm-linux-gnueabihf/
         To include and update PATH :      
```
tar -xf gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf.tar.xz
export PATH=/<install_path>/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf/bin:$PATH
arm-linux-gnueabihf -v
```
    
2. Set environment for Cross-Compilation:   
```
export CROSS_COMPILE=arm-linux-gnueabihf-
export ARCH=arm
```

Now to build and generate uboot.elf:
```
make zynq_zc702_defconfig
make
```

Now, u-boot.elf is available in the top directory. 
To make mkimage available in other steps, it is recommended to add the tools directory to your $PATH.
```
cd tools
export PATH=`pwd`:$PATH
```

Update .bif file:
```
/* Linux */
the_ROM_image:
{
    [bootloader] xsct_ws/zynq_fsbl/Release/zynq_fsbl.elf
    xsct_ws/spec7_custom/hw/spec7_custom.bit
    u-boot-xlnx/u-boot.elf
}
```

Generate Boot.bin image:
```
bootgen -image xsct_flow.bif -arch zynq -w -o BOOT.bin
```

4] Building Linux-Images:
1. Sources :
```
git clone https://github.com/Xilinx/linux-xlnx.git
cd linux-xlnx
git checkout xilinx-v2019.2.01 
```


   2. Set environment for Cross-Compilation:   
```
export CROSS_COMPILE=arm-linux-gnueabihf-
export ARCH=arm
```




